Richard Wiktorowicz
Zach Herr
Intermediate Object Oriented Programming
12/10/12

Features (extra credit):

Levels, locations, monsters, and items all read in from raw resource;
can easily be updated, changed, and added to.

Coin denominations.

AsyncTask monsters.


Remnants of 2a+b:

	Over the course of developing this project, we have tried to 
follow some of the design patterns that we discussed in class.  One of 
these design patterns is the single responsibility principle, which 
states that each class should only have one reason to change.  In 
other words, it should only have a single functionality.  We try to 
incorporate this in our project by having each portion of our game 
have its own class.  For example, we have a Player class which handles 
everything about the player, a location class which holds information 
about each location and it's neighbors, and a level class that holds 
information about the level as a whole, on top of having overlying 
interfaces to improve the extendibility of the code.

	In terms of the interface segregation principle, our program 
makes sure that we have interfaces each of the functionalities of our 
program.  Our interfaces are divided into a bunch of smaller 
interfaces that do specific tasks.  An over-arching interface for 
Actor is eventually split down into a concrete Player class in order 
to achieve ISP. For example, some of our classes have the ability to 
take on specific roles, and we have an interface for each of these 
roles.  That way, what users need is always available to them, nothing 
more. These �roles� play into the idea of favoring composition over 
inheritance, as according to 
http://userpages.umbc.edu/~tarr/dp/lectures/OOPrinciples.pdf

	We use the decorator principle in our program mainly through the 
Android Framework and their use of the View along with its children. 
We can at any point add our custom GameView as a child to the �root� 
class in order to achieve the decoration we need at this point in the 
project. Later, various plugin classes will support having items at 
each location.

	We use the observer pattern when dealing with the terrain and 
level creation.  When moving to different squares, the code gets 
updated with the location of the player.  Once the player is in a new 
location, the view gets updated to show our player on a new square.  
This is also used when we change levels.  Once the player is on a 
certain square, the program then knows that we are now able to move up 
a level, and updates the view accordingly.  When the level is changed, 
the view knows that it is now invalidated and needs to redraw itself.

For future expansion, we need to think of a better way to handle level 
switching, as well as the ability to have a �Google Map� like level, 
versus a discrete size. As far as Location enhancements go, we will 
have to implement the ability for Player to see what items a Location 
is composed of. To achieve this, it would make sense for both Location 
and Player to be composed of an Inventory.

	A lot of our work together consisted of mostly sketching out what 
we were going to do.  There were very little instances where we would 
just start coding without first discussing what we were doing.  We 
held multiple skype meetings weekly where we would go through the code 
and actually code stuff out together.  We used this time to figure out 
and break down each step that we needed to accomplish.  A lot of our 
test code dealt with making sure that the view was working the way we 
intended it to.  An important test was to make sure that when users 
clicked within the vicinity of a square that it reacted accordingly. 
The functional requirements were mostly handled by Zach while non-
functional taken care of by Rich, although we intermittently switched 
at points that we felt we could do better. For example, if someone 
developed a class, then they would also be responsible for writing 
test code with it.
_________________________________________________________________

What, if anything, do the various smaller objects have in common? What 
would be a good way to capture this commonality? Is this a permanent 
or temporary commonality?

 - All smaller objects would be part of an item class where they would 
share attributes and descriptions.  This would be a permanent 
commonality since items would not really change throughout the game.  
While small objects share an item's class, they would be separated in 
terms of tools, weapons, health items.


Are players related to or associated with small objects? If so, how? 
Are these permanent or temporary associations?

 - Certain players might only be able to carry certain types of 
objects.  These can be permanent or temporary depending on various 
factors throughout the game. Certain items the player might need on 
them at all times, which would be permanent, but some objects can be 
dropped or picked up in various locations which makes them temporary.


Are location cells related to or associated with actors, fixtures, and 
other objects? If so, how? Are these permanent or temporary 
associations?

 - Certain rooms might only house certain objects or certain monsters.  
However, there could be items within a room that can be moved to other 
rooms, which makes these certain associations, temporary.

Are location cells related to or associated with each other? If so, 
how? Are these permanent or temporary associations?

 - Location cells may be associated with each other meaning that you 
would have to do something in one location that would affect another 
location.  This would then be permanent.



