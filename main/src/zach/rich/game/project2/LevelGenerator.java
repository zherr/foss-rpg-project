package zach.rich.game.project2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;

import android.content.Context;
import android.content.res.Resources;
import android.os.Handler;
import android.util.Log;
import zach.rich.game.project2.model.Armor;
import zach.rich.game.project2.model.Coin;
import zach.rich.game.project2.model.DefaultItem;
import zach.rich.game.project2.model.DefaultLocation;
import zach.rich.game.project2.model.Direction;
import zach.rich.game.project2.model.Field;
import zach.rich.game.project2.model.Food;
import zach.rich.game.project2.model.Game;
import zach.rich.game.project2.model.Level;
import zach.rich.game.project2.model.Location;
import zach.rich.game.project2.model.LocationRole;
import zach.rich.game.project2.model.Monster;
import zach.rich.game.project2.model.Player;
import zach.rich.game.project2.model.Swamp;
import zach.rich.game.project2.model.Water;
import zach.rich.game.project2.model.Weapon;

public final class LevelGenerator implements Runnable {

	final Game game;
	final Context context;
	final int levelResource;
	final int mapResource;
	final int itemResource;
	final Player player;

	private final Handler hdlr = new Handler();
	private final Runnable makeGame = new Runnable() {

		@Override
		public void run() {
			try {
				makeGame();

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	};

	private volatile boolean done;

	LevelGenerator(Context context, Game game, int levelResource,
			int mapResource, int itemResource, Player player) {
		this.game = game;
		this.context = context;
		this.levelResource = levelResource;
		this.mapResource = mapResource;
		this.itemResource = itemResource;
		this.player = player;
	}

	public void done() {
		done = true;
	}

	@Override
	public void run() {
		while (!done) {
			hdlr.post(makeGame);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
			}
		}

	}

	void makeGame() throws IOException {
		final Resources resources = context.getResources();
		InputStream inputStream = resources.openRawResource(levelResource);
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				inputStream));

		int levels = Integer.parseInt(reader.readLine());// number of levels to
															// read in
		LocationRole realRole = null;
		/** First main loop for number of levels */
		for (int i = 0; i < levels; i++) {
			String name = "";
			int role;
			Level l;
			int numLevel = Integer.parseInt(reader.readLine());// this levels
																// number
			int numOfLocs = Integer.parseInt(reader.readLine());// number of
																// locs for this
																// level
			ArrayList<Monster> monsters = new ArrayList<Monster>(); // array
																	// list of
																	// monsters
																	// for this
																	// level
			LinkedList<Location> locations = new LinkedList<Location>();

			/** Second loop for # of locs for level */
			for (int j = 0; j < numOfLocs; j++) {
				name = reader.readLine();
				Log.d("Loc Name", name);
				int monstNum = Integer.parseInt(reader.readLine());
				Log.d("How many monsters?", String.valueOf(monstNum));
				role = Integer.parseInt(reader.readLine());
				switch (role) {
				case (3):
					realRole = new Swamp();
					break;
				case (2):
					realRole = new Water();
					break;
				case (1):
					realRole = new Field();
					break;
				default:
					realRole = null;
					break;
				}
				float left = Float.parseFloat(reader.readLine());
				float top = Float.parseFloat(reader.readLine());
				float right = Float.parseFloat(reader.readLine());
				float bottom = Float.parseFloat(reader.readLine());
				DefaultLocation temp = new DefaultLocation(name, realRole,
						left, top, right, bottom, numLevel);
				locations.add(temp);
				for (int z = 0; z < monstNum; z++) {
					Monster tempMon = new Monster(reader.readLine(), temp);
					Log.d("Monster Name", tempMon.getName());
					monsters.add(tempMon);
					temp.enter(tempMon);
				}

			}
			l = new Level(player, monsters, locations);
			game.addLevel(l);
			Log.d("ADDED LEVEL", "" + l.toString());
		}

		reader.close();

		/**
		 * Make mappings
		 */
		final Resources resources1 = context.getResources();
		InputStream inputStream1 = resources1.openRawResource(mapResource);
		BufferedReader reader2 = new BufferedReader(new InputStreamReader(
				inputStream1));

		/** First loop per level */
		for (int i = 0; i < levels; i++) {
			int mappings = Integer.parseInt(reader2.readLine());

			/** Second loop for locations per level */
			for (int j = 0; j < mappings; j++) {
				String levelName = reader2.readLine();
				Log.d("This Level", levelName);
				Log.d("Actual Location", game.getLevels().get(i)
						.listLocations().get(0).getname());
				Location toMap = game.getLevels().get(i).getLocation(levelName);
				// Log.d("Loc to Map: ", toMap.getname());
				int num = Integer.parseInt(reader2.readLine());
				/** Third loop for mappings per location */
				for (int z = 0; z < num; z++) {
					String dir = reader2.readLine();
					Direction realDir = null;
					for (Direction d : Direction.values()) {
						// Log.d("Direction", d.toString());
						if (d.toString().equals(dir)) {
							realDir = d;
							// Log.d("makeMap", "found DIRECTION match for " +
							// toMap.getname() + " to " + dir);
							break;
						}
					}

					int toLook = i;
					if (realDir == null) {
						Log.d("WARNING", "realDir is NULL");
					} else {
						if (dir.equals("UP")) {
							toLook = i + 1;
						} else if (dir.equals("DOWN")) {
							toLook = i - 1;
						}
					}

					String neighbor = reader2.readLine();
					Location realNeigh = null;
					for (Level l : game.getLevels()) {
						for (Location loc : l.listLocations()) {
							if (loc.getname().equals(neighbor)) {
								realNeigh = loc;
								Log.d("makeMap",
										"found NEIGHBOR match for "
												+ toMap.getname() + " to "
												+ loc.getname());
								break;
							}
						}
					}
					toMap.addNeighbor(realDir, realNeigh);

				}
			}
		}
		reader2.close();

		/**
		 * Model items
		 */
		final Resources resources3 = context.getResources();
		InputStream inputStream3 = resources3.openRawResource(itemResource);
		BufferedReader reader3 = new BufferedReader(new InputStreamReader(
				inputStream3));

		int numOfLocs = Integer.parseInt(reader3.readLine());
		for (int j = 0; j < numOfLocs; j++) {
			String nameOfLocation = reader3.readLine();
			int numOfItems = Integer.parseInt(reader3.readLine());
			for (int i = 0; i < numOfItems; i++) {
				String name = reader3.readLine();
				String description = reader3.readLine();
				int weight = Integer.parseInt(reader3.readLine());

				int role = Integer.parseInt(reader3.readLine());

				for (Level level : game.getLevels()) {
					for (Location loc : level.listLocations()) {
						if (loc.getname().equals(nameOfLocation)) {
							switch (role) {
							case (0):// Weapon
								loc.getInventory().addItem(
										new DefaultItem(name, description,
												weight, new Weapon(Integer
														.parseInt(reader3
																.readLine()))));
								break;
							case (1):// Armor
								loc.getInventory().addItem(
										new DefaultItem(name, description,
												weight, new Armor(Integer
														.parseInt(reader3
																.readLine()))));
								break;
							case (2):// Food
								loc.getInventory().addItem(
										new DefaultItem(name, description,
												weight, new Food(Integer
														.parseInt(reader3
																.readLine()))));
								break;
							case (3):// Coin
								loc.getInventory().addItem(
										new DefaultItem(name, description,
												weight, new Coin(Integer
														.parseInt(reader3
																.readLine()))));
								break;
							default:
								loc.getInventory().addItem(
										new DefaultItem(name, description,
												weight, null));
								break;
							}
						}
					}
				}
			}
		}
		reader3.close();

		game.doneModeling();

	}

}
