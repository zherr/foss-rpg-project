package zach.rich.game.project2.unimplemented;

public interface LivingThing{
		public LivingThingRole getRole();
		public boolean isAlive();
		public boolean isAggressive();
}
