package zach.rich.game.project2.unimplemented;

public interface LivingThingAttributeModel {
	public int getHealth();
	public int getMagic();
	public int getLevel();
	
}
