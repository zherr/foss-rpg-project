package zach.rich.game.project2;

import java.util.ArrayList;
import zach.rich.game.project2.model.Direction;
import zach.rich.game.project2.model.Game;
import zach.rich.game.project2.model.Item;
import zach.rich.game.project2.model.Location;
import zach.rich.game.project2.model.Player;
import zach.rich.game.project2.view.GameView;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Vibrator;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Button;
import android.widget.Toast;
import android.app.Dialog;

public class RpgController extends FragmentActivity{

	private static final class OnTouchListener implements View.OnTouchListener {

		private final Game game;

		OnTouchListener(Game game) {
			this.game = game;
		}

		@Override
		public boolean onTouch(View v, MotionEvent event) {
			
			float xTouch = event.getX();
			float yTouch = event.getY();
			game.getWithin(xTouch, yTouch);
			
			Log.d("TOUCHED", "OKAY");

			return false;
		}
	}
	
	MediaPlayer ourSong;

	Vibrator myVib; // vibrate object
	
	/** The application model for main level */
	final Game mainLevelModel = new Game(new Player("Legolas", null));

	/** The application View */
	GameView gameView;

	/** Level Generator */
	LevelGenerator levelGenerator;

	/** Called when created */
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		gameView = new GameView(this, mainLevelModel);

		gameView.setOnCreateContextMenuListener(this);

		// install the view
		setContentView(R.layout.activity_rpg_controller);

		ourSong = MediaPlayer.create(RpgController.this, R.raw.andwegotolder);
		ourSong.setLooping(true);
		ourSong.start();
		myVib = (Vibrator) this.getSystemService(VIBRATOR_SERVICE); // initialize vibrate
		
		((LinearLayout) findViewById(R.id.root)).addView(gameView, 0);

		gameView.setOnTouchListener(new OnTouchListener(mainLevelModel));

		// wire up controller
		((Button) findViewById(R.id.button1))
				.setOnClickListener(new Button.OnClickListener() {
					@Override
					public void onClick(View v) {
						
						Location cur = mainLevelModel.getMainPlayer().getCurrentCell();
						cur.leave(mainLevelModel.getMainPlayer());
						mainLevelModel.getMainPlayer().setCurrentCell(cur.listNeighbors().get(Direction.UP));
						cur.listNeighbors().get(Direction.UP).enter(mainLevelModel.getMainPlayer());

						checkButtons(); 
						gameView.invalidate();
					}
				});

		((Button) findViewById(R.id.button2))
				.setOnClickListener(new Button.OnClickListener() {
					@Override
					public void onClick(View v) {
						mainLevelModel.getMainPlayer().getCurrentCell().leave(mainLevelModel.getMainPlayer());
						Location cur = mainLevelModel.getMainPlayer().getCurrentCell();
						cur.leave(mainLevelModel.getMainPlayer());
						mainLevelModel.getMainPlayer().setCurrentCell(cur.listNeighbors().get(Direction.DOWN));
						cur.listNeighbors().get(Direction.DOWN).enter(mainLevelModel.getMainPlayer());

						checkButtons();
						gameView.invalidate();
					}
				});

		mainLevelModel
				.setLocationChangeListener(new Game.LocationChangeListener() {
					@Override
					public void onLocationChange() {

						((Button) findViewById(R.id.button1)).setEnabled(false);
						((Button) findViewById(R.id.button2)).setEnabled(false);						
						
						gameView.invalidate();
						Log.d("View", "INVALIDATED");
					}

					@Override
					public void onUpChange() {
						((Button) findViewById(R.id.button1)).setEnabled(true);
						gameView.invalidate();
					}

					@Override
					public void onDownChange() {
						((Button) findViewById(R.id.button2)).setEnabled(true);
						gameView.invalidate();
					}

					@Override
					public void onLevelChange() {
						if(mainLevelModel.getMainPlayer().getCurrentCell().listNeighbors().containsKey(Direction.UP)
								&& mainLevelModel.getMainPlayer().getCurrentCell().listNeighbors().containsKey(Direction.DOWN)){
							((Button) findViewById(R.id.button1)).setEnabled(true);
							((Button) findViewById(R.id.button2)).setEnabled(true);
						}
						else if(mainLevelModel.getMainPlayer().getCurrentCell().listNeighbors().containsKey(Direction.UP)){
							((Button) findViewById(R.id.button1)).setEnabled(true);
							((Button) findViewById(R.id.button2)).setEnabled(false);
						}
						else if(mainLevelModel.getMainPlayer().getCurrentCell().listNeighbors().containsKey(Direction.DOWN)){
							((Button) findViewById(R.id.button1)).setEnabled(false);
							((Button) findViewById(R.id.button2)).setEnabled(true);
						}

						gameView.invalidate();

					}

					@Override
					public void onMonstersChange(Boolean nextTo) {
						if(nextTo){
							myVib.vibrate(50);
						}
						gameView.invalidate();
					}

					@Override
					public void onWinOrLoseFight(Boolean win) {
						if(!win){
							Toast.makeText(getApplicationContext(), "YOU HAVE DIED AND LOST THE GAME", Toast.LENGTH_LONG).show();
							finish();
						}
					}
				});
		
		gameView.setOnLongClickListener(new View.OnLongClickListener() {
			
			@Override
			public boolean onLongClick(View v) {

				final CreateLocationInventoryDialogue dialogue = new CreateLocationInventoryDialogue();
				dialogue.show(getFragmentManager(), null);

				return true;
			}
		});

		/**
		 * Generate level
		 */
		{

			levelGenerator = new LevelGenerator(this, mainLevelModel,
					R.raw.main, R.raw.mainmap, R.raw.items,
					mainLevelModel.getMainPlayer());
			new Thread(levelGenerator).start();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			levelGenerator.done();
		}

	}
	
	@Override
	public void onStop(){
		super.onStop();
		ourSong.release();
		mainLevelModel.getAsync().cancel(true);
	}
	
	@Override
	public void onPause(){
		ourSong.release();
		super.onPause();
		ourSong.release();
		mainLevelModel.getAsync().cancel(true);
	}
	
	@Override
	public void onResume(){
		super.onResume();
		ourSong.release();
		ourSong = MediaPlayer.create(RpgController.this, R.raw.andwegotolder);
		ourSong.setLooping(true);
		ourSong.start();
	}
	
	@Override
	public void onStart(){
		super.onStart();		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_layout, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
			case(R.id.exit):
				finish();
			case(R.id.inventory):
				final CreatePlayerInventoryDialogue dialog = new CreatePlayerInventoryDialogue();
			dialog.show(getFragmentManager(), null);
			default:
				return super.onOptionsItemSelected(item);
		}
	}
	
	/**
	 * Checks location and level
	 */
	public void checkButtons(){
		if(mainLevelModel.getMainPlayer().getCurrentCell().listNeighbors().containsKey(Direction.UP)
				&& mainLevelModel.getMainPlayer().getCurrentCell().listNeighbors().containsKey(Direction.DOWN)){
			((Button) findViewById(R.id.button1)).setEnabled(true);
			((Button) findViewById(R.id.button2)).setEnabled(true);
		}
		else if(mainLevelModel.getMainPlayer().getCurrentCell().listNeighbors().containsKey(Direction.UP)){
			((Button) findViewById(R.id.button1)).setEnabled(true);
			((Button) findViewById(R.id.button2)).setEnabled(false);
		}
		else if(mainLevelModel.getMainPlayer().getCurrentCell().listNeighbors().containsKey(Direction.DOWN)){
			((Button) findViewById(R.id.button1)).setEnabled(false);
			((Button) findViewById(R.id.button2)).setEnabled(true);
		}
		gameView.invalidate();
	}
	
	/**
	 * Creates the inventory dialog for the location
	 * @author Zach
	 *
	 */
	public final class CreateLocationInventoryDialogue extends DialogFragment{
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			final ArrayList<Item> ar = mainLevelModel.getMainPlayer().getCurrentCell().getInventory().getitems();
			Log.d("INVENTORY SIZE", "" + ar.size());
			final CharSequence[] cs = new CharSequence[ar.size()];
			for(int i = 0; i < ar.size(); i++){
				cs[i] = ar.get(i).getName();
			}
			AlertDialog.Builder builder = new AlertDialog.Builder(RpgController.this);
			
			builder.setTitle("Select item(s)")
					.setItems(cs, new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Item toRemove = ar.get(which);
						try{
							mainLevelModel.getMainPlayer().getInventory().
								addItem(toRemove);
							
							if(true){Toast.makeText(getApplicationContext(), "Description: " + 
									toRemove.getDescription() , Toast.LENGTH_LONG).show();}
							
							if(ar.get(which).getName().equals("Cheeseburger")){
								Toast.makeText(getApplicationContext(), "YOU WIN", Toast.LENGTH_LONG).show();
								finish();
							}
							
							mainLevelModel.getMainPlayer().getCurrentCell().
							getInventory().removeItem(toRemove);
							
						}catch(ArrayIndexOutOfBoundsException ex){
							Toast.makeText(getApplicationContext(), 
									"Sorry, this item is too heavy to pick up", Toast.LENGTH_SHORT).show();
							
						}
						
						
					}
				});
			return builder.create();
		}
	}
	
	/**
	 * Creates the inventory dialog for the player
	 * @author Zach
	 *
	 */
	public final class CreatePlayerInventoryDialogue extends DialogFragment{
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			final ArrayList<Item> ar = mainLevelModel.getMainPlayer().getInventory().getitems();
			Log.d("INVENTORY SIZE", "" + ar.size());
			final CharSequence[] cs = new CharSequence[ar.size()];
			for(int i = 0; i < ar.size(); i++){
				cs[i] = ar.get(i).getName();
			}
			AlertDialog.Builder builder = new AlertDialog.Builder(RpgController.this);
			
			builder.setTitle("Select Item to Drop")
					.setItems(cs, new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						mainLevelModel.getMainPlayer().getCurrentCell().getInventory().
						addItem(mainLevelModel.getMainPlayer().
								getInventory().removeItem(ar.get(which)));
					}
				});

			return builder.create();
		}
	}
}
