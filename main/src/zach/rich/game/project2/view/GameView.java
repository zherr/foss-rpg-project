package zach.rich.game.project2.view;

import zach.rich.game.project2.R;
import zach.rich.game.project2.model.Actor;
import zach.rich.game.project2.model.Game;
import zach.rich.game.project2.model.Location;
import zach.rich.game.project2.model.Monster;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.Log;
import android.view.View;

public class GameView extends View {

	private final Game game;
	//private final Player mainPlayer;
	private final Bitmap playerDraw, treasureChest, monster;
	
	public GameView(Context context, Game game) {
		super(context);
		this.game = game;
		//mainPlayer = p;
		setFocusable(true);	
		setMinimumWidth(500);
        setMinimumHeight(550);
        playerDraw = new BitmapFactory().decodeResource(getResources(), R.drawable.player);
        treasureChest = new BitmapFactory().decodeResource(getResources(), R.drawable.treasurechest);
        monster = new BitmapFactory().decodeResource(getResources(), R.drawable.warcraft_orc);
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec){
		setMeasuredDimension(
				getSuggestedMinimumWidth(),
				getSuggestedMinimumHeight());
	}
	
	@SuppressLint("DrawAllocation")
	@Override
	protected void onDraw(Canvas canvas){
		canvas.drawColor(Color.BLACK);
		
		Log.d("Called onDraw", "should color something");
		
		Paint paint = new Paint();
        for(Location loc : game.getLevels().get(game.getMainPlayer().getCurrentCell().getLevel()).listLocations()){

        	paint.setColor(loc.getRole().getColor());
        	canvas.drawRect(loc.getLeft(), loc.getTop(), loc.getRight(), loc.getBottom(), paint);
        	
        	if(loc.getInventory().getitems().size() >= 1){
        		paint.setColor(Color.RED);
        		canvas.drawBitmap(treasureChest, null, new RectF(loc.getLeft(), loc.getTop(), loc.getRight(), loc.getBottom()), paint);
        	}
        	if(loc.listOccupants().contains(game.getMainPlayer())){
        		paint.setColor(Color.RED);
        		canvas.drawBitmap(playerDraw, null, new RectF(loc.getLeft(), loc.getTop(), loc.getRight(), loc.getBottom()), paint);
        	}
        	for(Actor a : loc.listOccupants()){
        		if(a instanceof Monster){
        			paint.setColor(Color.RED);
            		canvas.drawBitmap(monster, null, new RectF(loc.getLeft(), loc.getTop(), loc.getRight(), loc.getBottom()), paint);
        		}
        	}
        }       
	}
}
