package zach.rich.game.project2.model;


import java.util.HashMap;
import java.util.ArrayList;

/**
 * Default location model
 * @author Zach
 */
public class DefaultLocation implements Location{
	
	private final String cellName;
	private HashMap<Direction, Location> neighbors;
	private ArrayList<Actor> actors;
	private final LocationRole role;
	private final DefaultInventory inventory;
	private final int level;

	private final float left, right, top, bottom;

    /**
     * Contructor
     * @param cellName The formal name of the location
     * @param left The left side of the rect
     * @param right The right side f the rect
     * @param top The top side of the rect
     * @param bottom The bottom side of the rect
     */
	public DefaultLocation(String cellName, LocationRole role, float left, float top, float right, float bottom, int level) {
		this.cellName = cellName;
		neighbors = new HashMap<Direction, Location>();
		actors = new ArrayList<Actor>();
		this.role = role;
		this.left = left;
		this.right = right;
		this.top = top;
		this.bottom = bottom;
		inventory = new DefaultInventory(Integer.MAX_VALUE);//Assume locations can hold any number of items
		this.level = level;
	}

	@Override
	public void addNeighbor(Direction direction, Location location) {
		neighbors.put(direction, location);		
	}
	
	/**
	 * Removes a neighbor from this Location
	 * @param location The neighbor to remove
	 */
	public void removeNeighbor(Location location){
		neighbors.remove(location);
	}
	
	@Override
	public HashMap<Direction, Location> listNeighbors() {
		return neighbors;
	}

	@Override
	public ArrayList<Actor> listOccupants() {
		return actors;
	}

	@Override
	public void enter(Actor actor) {
		actors.add(actor);
		System.out.println("location " + getCellName() + " entered by " + actor.toString());
	}

	@Override
	public void leave(Actor actor) {
		actors.remove(actor);
		System.out.println("location " + getCellName() + " exited by " + actor.toString());
	}
	
	public String getCellName(){
		return this.cellName;
	}
	
	@Override
	public String toString(){
		String temp = "";
		temp += "Cell name: " + getCellName() + "\n";
		//temp += "Exits: \n" + neighbors.toString();		
		return temp;
	}

	@Override
	public float getLeft() {
		return left;
	}

	@Override
	public float getRight() {
		return right;
	}

	@Override
	public float getTop() {
		return top;
	}

	@Override
	public float getBottom() {
		return bottom;
	}

	@Override
	public LocationRole getRole() {
		return role;
	}

	@Override
	public boolean getWithin(float x, float y) {
		return (left<=x && x<right) && (top<=y && y<bottom);
	}

	@Override
	public String getname() {
		return cellName;
	}

	@Override
	public Inventory getInventory() {
		return inventory;
	}

	@Override
	public int getLevel() {
		return level;
	}
}
