package zach.rich.game.project2.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import android.os.AsyncTask;
import android.util.Log;

public class Game {

	/** Location change listener */
	public interface LocationChangeListener{
		/**
		 * Listener for location change
		 */
		void onLocationChange();
		
		/**
		 * Listener for when player steps on level linked location going up
		 */
		void onUpChange();
		
		/**
		 * Listener for when player steps on level linked location going down
		 */
		void onDownChange();
		
		/**
		 * Listener for when the level of the player changes
		 */
		void onLevelChange();
		
		/**
		 * Listener for when a monster changes location
		 * @param whether the monster is next to the main player
		 */
		void onMonstersChange(Boolean nextTo);
		
		/**
		 * Listener to whether or not the player has won the fight
		 * @param win True if won, false if lost
		 */
		void onWinOrLoseFight(Boolean win);
	}
	
	private ArrayList<Level> levels;
	private LocationChangeListener locationChangeListener;
	//private final ArrayList<Monster> monsters;//TODO
	private final Player mainPlayer;
	private AsyncMonsters genMonst;
/*	private Timer timer;
	private TimerTask task;*/
	
	public Game(Player player){
		mainPlayer = player;
		//lastLevelChange = 0;
		levels = new ArrayList<Level>();
		//monsters = new ArrayList<Monster>();//TODO
		//monsters.add(new Monster("Orc", null));//TODO
/*		task = new TimerTask() {
			
			@Override
			public void run() {
				notifyListener();				
			}
		};*/
	}
	
	/**
	 * Adds a level
	 * @param l The level to add
	 */
	public void addLevel(Level l){
		levels.add(l);
	}
	
	/**
	 * Gets the levels in the game
	 * @return The list of levels in the game
	 */
	public ArrayList<Level> getLevels(){
		return levels;
	}
	
/*	*//**
	 * Sets the number level of the up or down move
	 * @param c The change in level
	 *//*
	public void setLastLevelChange(int c){
		lastLevelChange = c;
	}
	
	*//**
	 * Get the last changed level
	 * @return The integer change of level
	 *//*
	public int getLastLevelChange(){
		return lastLevelChange;
	}*/
	
	/**
	 * @param l set the change listener
	 */
	public void setLocationChangeListener(LocationChangeListener l){
		locationChangeListener = l;
	}
	
	/**
     * Called when the player touches somewhere on the level's canvas
     * @param x The x-coord of the touch
     * @param y The y-coored of the touch
     */
    public void getWithin(float x, float y){

		Location pLoc = mainPlayer.getCurrentCell();
				
		if(!(pLoc.getWithin(x, y)) && pLoc != null){//TODO eventually have case to handle "self-click"
						
			Log.d("Is player within clicked sell?", String.valueOf(pLoc.getWithin(x, y)));
			
			HashMap<Direction, Location> locMap = pLoc.listNeighbors();
			for(Direction d : Direction.values()){
				//if(locMap.get(d) != null){Log.d("Level - getWithin", "a Neighbor: " + locMap.get(d).getBottom());}
				if(locMap.get(d) != null && locMap.get(d).getWithin(x, y)){
					Log.d("Level - getWithin", "locMap: " + locMap.get(d).toString());
					mainPlayer.setCurrentCell(locMap.get(d));
					locMap.get(d).enter(mainPlayer);
					pLoc.leave(mainPlayer);
					notifyListener();
					//notifyLevelListener();
					/** Enable buttons if up or down direction is there*/
					if(mainPlayer.getCurrentCell().listNeighbors().containsKey(Direction.UP)){
						notifyUpListener();
						//return 1;
					}
					if(mainPlayer.getCurrentCell().listNeighbors().containsKey(Direction.DOWN)){
						notifyDownListener();
						//return -1;
					}
					//return 0;
				}
			}
		}
    }
    
    /**
     * Notifies the listener of any location movement
     */
    private void notifyListener(){
    	if (null != locationChangeListener){
    		locationChangeListener.onLocationChange();
    	}
    }
    
    /**
     * Notifies the listener of any move onto a location
     * in which the player may be ble to go up
     */
    private void notifyUpListener(){
    	if (null != locationChangeListener){
    		locationChangeListener.onUpChange();
    	}
    }
    
    /**
     * Notifies the listener of any move onto a location
     * in which the player may be able to move down
     */
    private void notifyDownListener(){
    	if (null != locationChangeListener){
    		locationChangeListener.onDownChange();
    	}
    }
    
    /**
     * Notifies listener of any move by a monster
     */
    private void notifyMonstersChange(Boolean nextTo){
    	if (null != locationChangeListener){
    		locationChangeListener.onMonstersChange(nextTo);
    	}
    }
    
    private void notifyIfWinOrLoseFight(Boolean won){
    	if( null != locationChangeListener){
    		locationChangeListener.onWinOrLoseFight(won);
    	}
    }
    
    /**
     * Gets the main player of the level/game
     * @return The main player
     */
    public Player getMainPlayer(){
    	return mainPlayer;
    }
    
    /**
     * Returns the list of monsters
     * @return The array list of monsters
     */
/*    public ArrayList<Monster> getMonsters(){
    	return monsters;
    }*/
    
    /**
     * Gets the asynctask for gen monsters
     * @return The asynctask
     */
    public AsyncMonsters getAsync(){
    	return genMonst;
    }

    /**
     * Tells the listener if a monster is next to the main player
     * @param nextTo True if next to player, false otherwise
     */
    public void pleaseUpdateMonster(Boolean nextTo){
    	notifyMonstersChange(nextTo);
    }
    
    /**
     * Tells the listener if the player won the fight
     * @param win True if player won, false otherwise
     */
    public void theFight(Boolean win){
    	notifyIfWinOrLoseFight(win);
    }
    
    /**
     * Called once model is finally put together
     */
    public void doneModeling(){
    	mainPlayer.setCurrentCell(levels.get(1).listLocations().get(0));
    	Log.d("doneModeling", "level 1: " + levels.get(1).listLocations().get(0).getname());
		mainPlayer.getCurrentCell().enter(mainPlayer);

		genMonst = new AsyncMonsters(this);
		genMonst.execute();

    	notifyListener();
    }
	
}
