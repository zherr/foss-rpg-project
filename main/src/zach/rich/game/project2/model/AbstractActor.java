package zach.rich.game.project2.model;

/**
 * Abstract class for an Actor
 * @author Zach
 *
 */
public abstract class AbstractActor implements Actor{
	
	private final String name;
	private Location currentLocation;
	private final DefaultInventory inventory;
	private int health;
	private static final int damage = 100;
	
	/**
	 * Constructor
	 * @param name The name of the actor
	 * @param start The starting location for the actor
	 */
	public AbstractActor(String name, Location start){
		this.name = name;
		this.currentLocation = start;
		inventory = new DefaultInventory(100);
		health = 150;
	}
	
	@Override
	public String getName(){
		return name;
	}
	
	@Override
	public void setCurrentCell(Location location){ 
		currentLocation = location; 
	}
	
	@Override
	public Location getCurrentCell() {
		return currentLocation; 
	}
	
	@Override
	public Inventory getInventory(){
		return inventory;
	}
	
	@Override
	public int getHealth(){
		return health + inventory.foodBuff();
	}
	
	@Override
	public void alterHealth(int damageOrIncrease){
		health += damageOrIncrease;
	}
	
	@Override
	public int getBaseDamage(){
		return damage;
	}
}
