package zach.rich.game.project2.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.TreeSet;

public interface Location {
	
	/**
	 * Gets the role of this location
	 * @return LocationRole 
	 */
	public LocationRole getRole();
	
	/**
	 * Adds a neighbor to a certain direction
	 * @param location The cell neighbor
	 * @param direction The enum direction to place the cell
	 */
	public void addNeighbor(Direction direction, Location location);
	
	/**
	 * Lists all of the neighbors adjacent to this cell
	 * @return The List of neighbors
	 */
	public HashMap<Direction, Location> listNeighbors();
	
	/**
	 * Lists the occupants in this cell
	 * @return The List of occcupants
	 */
	public ArrayList<Actor> listOccupants();
	
	/**
	 * This method allows an actor to enter the cell
	 */
	public void enter(Actor actor);
	
	/**
	 * This method allows an actor to leave this cell
	 */
	public void leave(Actor actor);
	
	/**
	 * Returns the left coord of rect
	 */
	public float getLeft();
	
	/**
	 * Returns the right coord of rect
	 */
	public float getRight();
	
	/**
	 * Returns the top coord of rect
	 */
	public float getTop();
	
	/**
	 * Returns bottom coord of rect
	 */
	public float getBottom();
	
	/**
	 * Returns true if points are within the location
	 * @param x
	 * @param y
	 * @return
	 */
	public boolean getWithin(float x, float y);
	
	/**
	 * Gets the name of the cell
	 * @return The String name of the cell
	 */
	public String getname();
	
	/**
	 * Gets this location's inventory
	 * @return The inventory of this location
	 */
	public Inventory getInventory();
	
	/**
	 * Gets the level the location is on
	 * @return
	 */
	public int getLevel();

}
