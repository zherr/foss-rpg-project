package zach.rich.game.project2.model;

public class Battle{

	private final Player player;
	private final Monster monster;
	
	public Battle(Player p, Monster m){
		player = p;
		monster = m;
	}

	/**
	 * Determines the winner of a fight based the player
	 * vs. the monster
	 * @return True if player won, false otherwise
	 */
	public boolean fight(){
				
		int playerDefense = player.getInventory().getTotalDefense();
		int playerOffense = player.getInventory().getTotalOffense() + player.getBaseDamage();
		System.out.println("player defense: " + playerDefense);
		System.out.println("player offense: " + playerOffense );
		
		while(player.getHealth() > 0 && monster.getHealth() > 0){
			
			int damageByMonster = monster.getBaseDamage();
			damageByMonster -= playerDefense;
			System.out.println("damage by monster: " + damageByMonster);
			/** Player always attacks first */
			monster.alterHealth(-playerOffense);
			System.out.println("this monster's health: " + monster.getHealth());
			if(monster.getHealth() <= 0){
				System.out.println("MONSTER DIED");
				return true;
			}
			player.alterHealth(-damageByMonster);
			if(player.getHealth() <= 0){
				System.out.println("PLAYER DIED");
				return false;
			}	
		}
			
		return player.getHealth() > 0;
	}
}
