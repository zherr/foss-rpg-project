package zach.rich.game.project2.model;

public class Coin extends AbstractItemRole {

	public int denomination;
	
	public Coin(int denom) {
		super("Coin");
		denomination = denom;
	}
	
	public int getDenom(){
		return denomination;
	}

}
