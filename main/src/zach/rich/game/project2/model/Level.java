package zach.rich.game.project2.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;


public class Level {
	
	private LinkedList<Location> locations = new LinkedList<Location>();
	private  List<Location> safeLocations = Collections.unmodifiableList(locations);
	private final Player mainPlayer;
	private final ArrayList<Monster> monsters;

	public Level(Player player, ArrayList<Monster> monList, LinkedList<Location> locs){
		mainPlayer = player;
		monsters = monList;
		locations = locs;
		safeLocations = locs;
	}
	
	/**
	 * List all of the locations in the level
	 * @return The List of all the Locations
	 */
    public List<Location> listLocations(){
    	return safeLocations;
    }

    /**
     * Adds a location to this Level list of locations
     * @param name The name of the location
     * @param role The role the location plays
     * @param left Left bound of loc
     * @param top Top bound of loc
     * @param right Right bound of loc
     * @param bottom Bottom bound of loc
     * @param level The level of the location
     */
    public void addLocation(String name, LocationRole role, float left, float top, float right, float bottom, int level) {
    	locations.add(new DefaultLocation(name, role, left, top, right, bottom, level));
    }
    
    /**
     * Returns the location based off the name
     * @param name The location to look for
     * @return The location or null
     */
    public Location getLocation(String name){
    	Location l = null;
    	for(Location loc : safeLocations){
    		if(loc.getname().equals(name)){
    			l = loc;
    			return l;
    		}
    	}
    	
    	return l;
    }
    
    /**
     * Clears all of the locations from the level
     */
    public void clearLocations(){
    	locations.clear();
    }
    
    /**
     * Gets the main player of the level/game
     * @return The main player
     */
    public Player getMainPlayer(){
    	return mainPlayer;
    }
    
    /**
     * Gets this level's list of monsters
     * @return the ArrayList of monsters
     */
    public ArrayList<Monster> getMonsters(){
    	return monsters;
    }
    
    /**
     * Sets the player's location to the above cell
     */
    public void setMainPlayerUpPeriph(){
    	HashMap<Direction, Location> locMap = mainPlayer.getCurrentCell().listNeighbors();
    	mainPlayer.setCurrentCell(locMap.get(Direction.UP));
    }
    
    /**
     * Sets the player's location to the below cell
     */
    public void setMainPlayerDownPeriph(){
    	HashMap<Direction, Location> locMap = mainPlayer.getCurrentCell().listNeighbors();
    	mainPlayer.setCurrentCell(locMap.get(Direction.DOWN));
    }
}
