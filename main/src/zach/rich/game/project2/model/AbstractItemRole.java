package zach.rich.game.project2.model;

public abstract class AbstractItemRole implements ItemRole{
	
	private String name;
	
	public AbstractItemRole(String name){
		this.name = name;
	}
	
	@Override
	public String getName(){
		return name;
	}

}
