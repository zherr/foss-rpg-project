package zach.rich.game.project2.model;


public class Armor extends AbstractItemRole{

	public int durability;
	
	public Armor(int durability) {
		super("Armor");
		this.durability = durability;
	}

	/**
	 * Gets the amount of durability left for the armor
	 * @return The integer number of the durability left
	 */
	public int getDurability(){
		return durability;
	}

}
