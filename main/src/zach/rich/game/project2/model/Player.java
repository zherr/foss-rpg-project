package zach.rich.game.project2.model;

/**
 * This class acts as the player
 * @author Zach
 * @author Richard
 *
 */
public class Player extends AbstractActor{

	
	/**
	 * Inherited contructor
	 * @param name The name of the player
	 * @param start The starting Location
	 */
	public Player(String name, Location start) {
		super(name, start);		
	}
	
	
	@Override
	public String toString(){
		return super.getName();
	}
}
