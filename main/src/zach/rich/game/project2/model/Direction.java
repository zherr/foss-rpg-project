package zach.rich.game.project2.model;

public enum Direction {
	NORTH, WEST, EAST, SOUTH, UP, DOWN
}
