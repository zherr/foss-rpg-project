package zach.rich.game.project2.model;

public class DefaultItem implements Item{

	private String name, description;
	private int weight;
	private final ItemRole itemRole;
	
	/**
	 * Contructor
	 * @param name The name of the Item
	 * @param description The description of the Item
	 * @param weight The integer weight of the item
	 * @param ir The role the Item plays
	 */
	public DefaultItem(String name, String description, int weight, ItemRole ir){
		this.name = name;
		this.description = description;
		this.weight = weight;
		itemRole = ir;
	}	
	
	@Override
	public int getWeight() {
		return weight;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public ItemRole getItemRole() {
		return itemRole;
	}
}
