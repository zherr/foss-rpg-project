package zach.rich.game.project2.model;

import java.util.ArrayList;

public interface Inventory {
	/**
	 * Gets the Items in this inventory.
	 * @return An ArrayList of Items
	 */
	public ArrayList<Item> getitems();
	
	/**
	 * Gets the capacity by weight that the inventory can hold.
	 * @return The integer capacity of the inventory.
	 */
	public int getCapacity();
	
	/**
	 * Gets the sum of all the weights of each item
	 * @return The integer sum of all the weights of the items
	 */
	public int getCurrentWeight();
	
	/**
	 * Attempts to add an item to the inventory
	 * @param item The item to add
	 * @throws ArrayIndexOutOfBoundsException when you try to add an element
	 * beyond the inventory's capactiy
	 */
	public void addItem(Item item) throws ArrayIndexOutOfBoundsException;
	
	/**
	 * Removes the item from the inventory
	 * @param item The item to remove
	 * @return The item just removed
	 */
	public Item removeItem(Item item);
	
	/**
	 * Gets the total sum of offensive points in inventory
	 * @return The sum of offensive items the actor has
	 */
	public int getTotalOffense();
	
	/**
	 * Gets the total sum of defensive points in the inventory
	 * @return The sum of defensive items the actor has
	 */
	public int getTotalDefense();
	
	/**
	 * Gets the buff of the food in the inventory
	 * @return The food buff
	 */
	public int foodBuff();
}
