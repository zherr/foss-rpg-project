package zach.rich.game.project2.model;

/**
 * Simple interface for an Actor
 * @author Zach
 *
 */
public interface Actor {
	
	/**
	 * This method sets the cell that the actor is currently in
	 * @param location The cell for the actor to be in
	 */
	public void setCurrentCell(Location location);
	
	/**
	 * Gets the cell that the actor is currently in
	 * @return The Cell that the actor is in
	 */
	public Location getCurrentCell();
	
	/**
	 * Gets the name of the actor
	 * @return String name of the actor
	 */
	public String getName();
	
	/**
	 * Gets the actor's inventory
	 * @return The actor's inventory
	 */
	public Inventory getInventory();
	
	/**
	 * Gets the actor's health
	 * @return The integer health amount
	 */
	public int getHealth();
	
	/**
	 * Increase or decrement the health of the player
	 * @param damageOrIncrease The increment or decrement
	 */
	public void alterHealth(int damageOrIncrease);
	
	/**
	 * Gets the base damage of the actor
	 * @return The base damage of the actor
	 */
	public int getBaseDamage();
	
}
