package zach.rich.game.project2.model;

public interface ItemRole {
	
	/**
	 * Gives a name to the role
	 */
	public String getName();
}
