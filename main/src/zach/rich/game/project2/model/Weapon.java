package zach.rich.game.project2.model;

public class Weapon extends AbstractItemRole{

	private int damage;
	
	public Weapon(int damage) {
		super("Weapon");
		this.damage = damage;
	}
	
	/**
	 * Get the amount of damage the weapon can do
	 * 
	 * @return The integer amount of damage the weapon can do
	 */
	public int getDamage(){
		return damage;
	}

}
