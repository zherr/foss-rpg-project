package zach.rich.game.project2.model;

import android.graphics.Color;

public class Water implements LocationRole {

	@Override
	public int getColor() {
		return Color.BLUE;
	}

	@Override
	public String getName() {
		return "water";
	}

	@Override
	public int getCodeName() {
		return 3;
	}

}
