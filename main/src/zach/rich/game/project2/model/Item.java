package zach.rich.game.project2.model;

public interface Item {
	public int getWeight();
	public String getName();
	public String getDescription();
	public ItemRole getItemRole();
}
