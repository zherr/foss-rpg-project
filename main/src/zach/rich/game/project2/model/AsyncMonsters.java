package zach.rich.game.project2.model;

import java.util.ArrayList;
import java.util.Random;
import android.os.AsyncTask;
import android.util.Log;

public final class AsyncMonsters extends AsyncTask<Void, Boolean[], Void> {

	private Game game;
	private Random rand;
	private Boolean[] checks = new Boolean[2];

	public AsyncMonsters(final Game g) {
		game = g;
		rand = new Random();
		checks[0] = false;
		checks[1] = true;
	}

	@Override
	protected Void doInBackground(Void... params) {

		while (true) {
			boolean check = false;
			if (isCancelled()) {
				break;
			}
			for (Monster m : game.getLevels()
					.get(game.getMainPlayer().getCurrentCell().getLevel())
					.getMonsters()) { // TODO change..
				if (game.getMainPlayer().getCurrentCell().getLevel() != m
						.getCurrentCell().getLevel()) {
					break;
				}
				try {
					Thread.sleep(rand.nextInt(1000) + 1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				checks[0] = check;
				publishProgress(checks);
				Location l = m.getCurrentCell();
				ArrayList<Direction> dirs = new ArrayList<Direction>(l
						.listNeighbors().keySet());
				Direction pick = null;
				if (dirs.size() > 0) {
					dirs.remove(Direction.UP);
					dirs.remove(Direction.DOWN);

					for (Direction d : dirs) {
						if (m.getCurrentCell().listNeighbors().get(d)
								.equals(game.getMainPlayer().getCurrentCell())
								|| m.getCurrentCell()
										.getname()
										.equals(game.getMainPlayer()
												.getCurrentCell().getname())) {
							check = true;
						}
					}
					if (check) {
						checks[0] = check;
						publishProgress(checks);
						if (m.getCurrentCell()
								.getname()
								.equals(game.getMainPlayer().getCurrentCell()
										.getname())) {
							Battle battle = new Battle(game.getMainPlayer(), m);
							if (battle.fight()) {
								m.setCurrentCell(null);
								l.leave(m);
								game.getLevels()
										.get(game.getMainPlayer()
												.getCurrentCell().getLevel())
										.getMonsters().remove(m);
								checks[1] = true;

							} else {
								checks[1] = false;
								publishProgress(checks);
							}
						}

						break;
					}

					pick = dirs.get(rand.nextInt(dirs.size()));
					m.setCurrentCell(m.getCurrentCell().listNeighbors()
							.get(pick));
					l.listNeighbors().get(pick).enter(m);
					l.leave(m);

				}
			}
		}

		return null;
	}

	@Override
	protected void onProgressUpdate(Boolean[]... values) {
		game.pleaseUpdateMonster(values[0][0]);
		game.theFight(values[0][1]);
	}

	@Override
	protected void onCancelled(Void result) {

	}
}
