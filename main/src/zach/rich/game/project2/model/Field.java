package zach.rich.game.project2.model;

import android.graphics.Color;

public class Field implements LocationRole {

	@Override
	public int getColor() {
		return Color.GREEN;
	}

	@Override
	public String getName() {
		return "field";
	}

	@Override
	public int getCodeName() {
		return 1;
	}

}
