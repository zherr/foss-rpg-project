package zach.rich.game.project2.model;


public class Food extends AbstractItemRole{
	
	private int healthValue;
	
	public Food(int hv){
		super("Food");
		healthValue = hv;
	}
	
	public int getHealthValue(){
		return healthValue;
	}

}
