package zach.rich.game.project2.model;

import java.util.ArrayList;

public class DefaultInventory implements Inventory {

	private ArrayList<Item> items;
	private int capacity;
	private int currentWeight;
	private int totalDefense;
	private int totalOffense;
	private int foodBuff;

	public DefaultInventory(int capacity) {
		this.capacity = capacity;
		items = new ArrayList<Item>();
	}

	@Override
	public ArrayList<Item> getitems() {
		return items;
	}

	@Override
	public String toString() {
		String temp = "Items: \n";
		for (int i = 0; i < items.size(); i++) {
			temp += items.get(i) + "\n";
		}
		return temp;
	}

	@Override
	public int getCapacity() {
		return capacity;
	}

	@Override
	public int getCurrentWeight() {
		return currentWeight;
	}

	@Override
	public void addItem(Item item) throws ArrayIndexOutOfBoundsException {
		if ((currentWeight + item.getWeight() < capacity)) {
			items.add(item);
			currentWeight += item.getWeight();
			if (item.getItemRole() instanceof Weapon) {
				totalOffense += ((Weapon) item.getItemRole()).getDamage();
			}
			if (item.getItemRole() instanceof Armor) {
				totalDefense += ((Armor) item.getItemRole()).getDurability();
			}
			if (item.getItemRole() instanceof Food) {
				foodBuff += ((Food) item.getItemRole()).getHealthValue();
			}
		} else {
			throw new ArrayIndexOutOfBoundsException();
		}
	}

	@Override
	public Item removeItem(Item item) {
		items.remove(item);
		currentWeight -= item.getWeight();
		if (item.getItemRole() instanceof Weapon) {
			totalOffense -= ((Weapon) item.getItemRole()).getDamage();
		}
		if (item.getItemRole() instanceof Armor) {
			totalDefense -= ((Armor) item.getItemRole()).getDurability();
		}
		if (item.getItemRole() instanceof Food) {
			foodBuff -= ((Food) item.getItemRole()).getHealthValue();
		}
		return item;
	}

	@Override
	public int getTotalOffense() {
		return totalOffense;
	}

	@Override
	public int getTotalDefense() {
		return totalDefense;
	}

	@Override
	public int foodBuff() {
		return foodBuff;
	}

}
