package zach.rich.game.project2.model;

import android.graphics.Color;

public class Swamp implements LocationRole {

	@Override
	public int getColor() {
		return Color.DKGRAY;
	}

	@Override
	public String getName() {
		return "swamp";
	}

	@Override
	public int getCodeName() {
		return 2;
	}

}
