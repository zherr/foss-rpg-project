package zach.rich.game.project2.model;

public interface LocationRole {
	 public int getColor();
	 public String getName();
	 public int getCodeName();
}
