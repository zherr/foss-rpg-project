package zach.rich.game.project2.test.player;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import zach.rich.game.project2.model.*;

public class PlayerTest {
	
	private Player p1;
	private Location loc;
	private Location loc2;

	@Before
	public void setUp() throws Exception {
		loc = new DefaultLocation("The Shire", new Field(), 1, 1, 1, 1, 0);
		loc2 = new DefaultLocation("Mordor", new Swamp(), 1, 1, 1, 1, 0);
		loc.addNeighbor(Direction.NORTH, loc2);
		loc2.addNeighbor(Direction.SOUTH, loc);
		p1 = new Player("Legolas", loc);
	}

	@Test
	public void testMove1() {
		assertTrue(p1.getCurrentCell().equals(loc));
		loc.enter(p1); // player starts in The Shire
		assertTrue(loc.listOccupants().contains(p1));
		p1.setCurrentCell(loc);
		assertTrue(p1.getCurrentCell().equals(loc));
	}
	
	@Test
	public void testMove2(){
		p1.setCurrentCell(loc2);
		assertTrue(p1.getCurrentCell().equals(loc2));
		loc.leave(p1);
		assertTrue(loc.listOccupants().isEmpty());
		
		assertFalse(loc2.listOccupants().contains(p1));
		loc2.enter(p1);
		assertTrue(loc2.listOccupants().contains(p1));
	}
	
	@Test
	public void testHealth(){
		assertEquals(150, p1.getHealth());
	}

}
