package zach.rich.game.project2.test.item;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import zach.rich.game.project2.model.Armor;
import zach.rich.game.project2.model.DefaultItem;
import zach.rich.game.project2.model.Food;
import zach.rich.game.project2.model.Weapon;

public class DefaultItemTest {
	
	private DefaultItem item1;
	private DefaultItem item2; 
	private DefaultItem item3; 
	
	@Before
	public void setUp() throws Exception {
		item1 = new DefaultItem("The Mega Axe", "This axe was once weilded by Gimli himself", 50, new Weapon(120));
		item2 =	new DefaultItem("Leather Vest", "A simple leather vest", 40, new Armor(45));
		item3 = new DefaultItem("Cheeseburger", "A gooey delicious cheeseburger", 3, new Food(35));
	}

	@Test
	public void testNames() {
		assertTrue(item1.getName().equals("The Mega Axe"));
		assertTrue(item2.getName().equals("Leather Vest"));
		assertTrue(item3.getName().equals("Cheeseburger"));
		
		assertTrue(item1.getDescription().equals("This axe was once weilded by Gimli himself"));
		assertTrue(item2.getDescription().equals("A simple leather vest"));
		assertTrue(item3.getDescription().equals("A gooey delicious cheeseburger"));				
	}
	
	@Test
	public void testWeight() {
		assertEquals(50, item1.getWeight());
		assertEquals(40, item2.getWeight());
		assertEquals(3, item3.getWeight());
	}
	
	@Test
	public void testRoles(){
		assertTrue(item1.getItemRole().getName().equals("Weapon"));
		assertTrue(item2.getItemRole().getName().equals("Armor"));
		assertTrue(item3.getItemRole().getName().equals("Food"));
		
		assertTrue(item1.getItemRole() instanceof Weapon);
		assertTrue(item2.getItemRole() instanceof Armor);
		assertTrue(item3.getItemRole() instanceof Food);
		
		assertTrue(((Weapon)item1.getItemRole()).getDamage() == 120);
		assertTrue(((Armor)item2.getItemRole()).getDurability() == 45);
		assertTrue(((Food)item3.getItemRole()).getHealthValue() == 35);
	}

}



