package zach.rich.game.project2.test.locationrole;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import android.graphics.Color;

import zach.rich.game.project2.model.*;

public class LocationRoleTest {

	private Water water;
	private Swamp swamp;
	private Field field;
	
	@Before
	public void setUp() throws Exception {
		water = new Water();
		swamp = new Swamp();
		field = new Field();
	}
	
	@Test
	public void testName() {
		assertTrue(water.getName().equals("water"));
		assertTrue(swamp.getName().equals("swamp"));
		assertTrue(field.getName().equals("field"));
	}

	@Test
	public void testColor() {
		assertTrue(water.getColor() == Color.BLUE);
		assertTrue(swamp.getColor() == Color.DKGRAY);
		assertTrue(field.getColor() == Color.GREEN);
	}
}
