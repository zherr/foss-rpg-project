package zach.rich.game.project2.test.inventory;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

import zach.rich.game.project2.model.Armor;
import zach.rich.game.project2.model.DefaultInventory;
import zach.rich.game.project2.model.DefaultItem;
import zach.rich.game.project2.model.Weapon;

public class DefaultInventoryTest {

	private DefaultInventory inven;
	
	@Before
	public void setUp() throws Exception {
		inven = new DefaultInventory(300);
	}

	@Test
	public void testGetters() {
		assertEquals(300, inven.getCapacity());
		assertEquals(0, inven.getCurrentWeight());
		assertTrue(inven.getitems().size() == 0);
	}
	
	@Test
	public void testCapacity(){
		assertEquals(0, inven.getCurrentWeight());
		assertTrue(inven.getitems().size() == 0);
		inven.addItem(new DefaultItem("Helm", "simple helmet", 120, new Armor(50)));
		assertEquals(120, inven.getCurrentWeight());
		assertTrue(inven.getitems().size() == 1);
		
		try{
			inven.addItem(new DefaultItem("MEGA HAMMER", "very large hammer", 200, new Weapon(300)));
		}catch(ArrayIndexOutOfBoundsException e){
			assertTrue(inven.getCurrentWeight() == 120);
			assertTrue(inven.getitems().size() == 1);
		}
		
	}
	
	@Test
	public void testRemoval(){
		DefaultItem item1 = new DefaultItem("Bow", "a simple bow", 40, new Weapon(60));
		
		inven.addItem(item1);
		assertTrue(inven.getCurrentWeight() == 40);
		assertTrue(inven.getitems().size() == 1);
		
		inven.removeItem(item1);
		assertTrue(inven.getCurrentWeight() == 0);
		assertTrue(inven.getitems().size() == 0);
		
		
	}

}
