package zach.rich.game.project2.test.location;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import zach.rich.game.project2.model.DefaultLocation;
import zach.rich.game.project2.model.Direction;
import zach.rich.game.project2.model.Swamp;
import zach.rich.game.project2.model.Water;

public class DefaultLocationTest {
	
	private DefaultLocation location_1;
	private DefaultLocation location_2;

	@Before
	public void setUp() throws Exception {
		location_1 = new DefaultLocation("location_1", new Swamp(), 1, 10, 30, 40, 0);
		location_2 = new DefaultLocation("location_2", new Water(), 1, 1, 1, 1, 0);
		
	}

	@Test
	public void testAddNeighbor() {
		location_1.addNeighbor( Direction.NORTH, location_2);
		assertTrue(location_1.listNeighbors().size() == 1);
		assertTrue(location_1.listNeighbors().get(Direction.NORTH).equals(location_2));
	}
	
	@Test
	public void testRemoveNeighbor() {
		location_1.removeNeighbor(location_2);
		assertTrue(location_1.listNeighbors().size() == 0);
	}
	
	@Test
	public void testCoord(){
		assertTrue(location_1.getLeft() == 1);
		assertTrue(location_1.getTop() == 10);
		assertTrue(location_1.getRight() == 30);
		assertTrue(location_1.getBottom() == 40);
	}
	
	@Test
	public void testLocRole(){
		assertTrue(location_1.getRole() instanceof Swamp);
	}
	
	@Test
	public void testWithin(){
		location_2 = new DefaultLocation("Fanghorn", new Swamp(), 225, 500, 275, 550, 0);
		assertTrue(location_2.getWithin(250, 525));
		assertFalse(location_2.getWithin(0, 0));
		assertTrue(location_2.getWithin(225, 549));
	}

}
