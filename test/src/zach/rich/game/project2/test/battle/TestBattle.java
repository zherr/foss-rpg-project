package zach.rich.game.project2.test.battle;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import zach.rich.game.project2.model.Battle;
import zach.rich.game.project2.model.Game;
import zach.rich.game.project2.model.Monster;
import zach.rich.game.project2.model.Player;

public class TestBattle {

	Battle battle1;
	Battle battle2;
	Battle battle3;

	Game game;
	Player mainPlayer;
	Monster m1, m2, m3;

	@Before
	public void setUp() throws Exception {
		mainPlayer = new Player("Player", null);
		game = new Game(mainPlayer);
		m1 = new Monster("Orc", null);
		m2 = new Monster("Orc", null);
		m3 = new Monster("Orc", null);
		battle1 = new Battle(mainPlayer, m1);
		battle2 = new Battle(mainPlayer, m2);
		battle3 = new Battle(mainPlayer, m3);
	}

	@Test
	public void testBattle() {
		assertTrue(mainPlayer.getHealth() == 150);
		assertTrue(m1.getHealth() == 150);
		assertTrue(battle1.fight());
		assertFalse(battle2.fight());
	}

}
